// @flow
class MyClass {
	myParam: string;

	constructor(param: string) {
		this.myParam = param;
	}

	myFunction(): string {
		return 1;
	}
};

const myInstance = new MyClass('12');
myInstance.myFunction();

const myOtherInstance: MyClass = new MyClass(23);
myOtherInstance.myFunction();

console.log(myOtherInstance.myParam);