// @flow
const isEven = (someNumber: number): boolean => someNumber%2 === 0;

const even: boolean = isEven(2);
const errorString: boolean = isEven('2');

//const multipleArgs = (arg1: number, arg2: number): number => arg1 * arg2;
const multipleArgs = (arg1: number, arg2: number, ...rest: Array<void>): number => arg1 * arg2;

const result = multipleArgs(1, 2, 3, 4, 5);

const anyFunction: Function = (value: string) => value;

const anyResult = anyFunction(10);