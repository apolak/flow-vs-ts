var MyClass = (function () {
    function MyClass(param) {
        this.myParam = param;
    }
    MyClass.prototype.myFunction = function () {
        return 1;
    };
    return MyClass;
}());
;
var myInstance = new MyClass('12');
myInstance.myFunction();
var myOtherInstance = new MyClass(23);
myOtherInstance.myFunction();
