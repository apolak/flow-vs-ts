var isEven = function (someNumber) { return someNumber % 2 === 0; };
var even = isEven(2);
var errorString = isEven('2');
