// @flow

interface Printable {
	print(): string;
}

interface Content {
	content: string;
}

interface OtherContent {
	content: string;
}

class Page implements Printable, Content {
	content: string;

	constructor(content: string) {
		this.content = content;
	}

	print(): string {
		return this.content;
	}
}

class Book implements Printable {
	title: string;
	content: string;

	constructor(title: string, content: string) {
		this.content = content;
		this.title = title;
	}

	print(): string {
		return this.content;
	}
}

class Website implements OtherContent {
	content: string;

	constructor(content: string) {
		this.content = content;
	}
}

const printables: Array<Printable> = [];
const contentables: Array<Content> = [];

printables.push(new Page('some page'));
printables.push(new Book('some title', 'some page'));
//printables.push(new Website('some title', 'some page'));

contentables.push(new Page('some page'));
contentables.push(new Book('some title', 'some page'));
contentables.push(new Website('some content'));

printables.forEach((printable: Printable): void => {
	printable.print();
});