class Movie {
	title: string
}

class Game {
	title: string;
}

let movie: Movie = new Movie();
let game: Game = movie;
let otherGame: Game = {
	title: "cool title"
};

type MovieObject = {
	title: string
}

type GameObject = {
	title: string
}

let movieObj: MovieObject = {
	title: 'some-title'
};

let gameObject: GameObject = {
	title: 'some-title'
};

let notCoolGame: GameObject = {
	year: 1999
};

let coolGame: GameObject = {
	title: 'cool',
	year: 1999
};

movieObj = gameObject;

