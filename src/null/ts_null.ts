const someMethod = (returnNull: boolean): string => {
	if (!returnNull) {
		return "string";
	}

	return null;
};

const myString: string = someMethod(false);
const thisShouldBeNull: string = someMethod(true);

type MyObject = {
	name: string,
	title: string,
	number?: number
};

const someObject: MyObject = {
	name: "My name",
	title: null
};