//@flow

type PricesDictionary = {
	//[key: string]: number
}

const prices: PricesDictionary = {
	netto: 200,
	brutto: 244
};

const myPrice: number = prices.netto;
const samePrice: number = prices['brutto'];

let tuple: [string, number, boolean] = ["foo", 0, true];

tuple[0] = 1;
tuple[1] = 1;
tuple[2] = true;