//@flow
import React from 'react';

type MyComponentProperties = {
	name: string
};

export default class MyComponent extends React.Component {
	props: MyComponentProperties;

	render() {
		return (
			<p>{ this.props.name }</p>
		);
	}
}
