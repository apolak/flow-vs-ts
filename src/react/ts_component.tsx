import * as React from 'react';

type MyComponentProperties = {
	name: string
}

export class MyComponent extends React.Component<MyComponentProperties, void> {
	static propTypes = {
		name: React.PropTypes.string
	};

	public render() {
		return (
			<p>{ this.props.name }</p>
		);
	}
}
